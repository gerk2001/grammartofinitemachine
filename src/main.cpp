﻿
#include <Windows.h>
#include <fmt/core.h>
#include <grammar.h>
#include <lrad.h>

int main()
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);

    Grammar grammar = Grammar();

    std::vector<std::pair<char, std::string>> prod_table = grammar.getListOfRules();
    fmt::print("\n\tПреобразованные правила грамматики:\n");
    for (size_t i = 0; i < prod_table.size(); i++)
        fmt::print("\t{} {}->{}\n", i, prod_table[i].first, prod_table[i].second);

    LRAN lran = LRAN(grammar);
    lran.printTable();

    LRAD lrad = LRAD(grammar, lran);
    lrad.analyzeStrings();

}

//TODO добавить метку квазистенциальности состояния и пересмотреть добавление новых состояний
//переместить колонку эпсилон-переходов в правый край таблицы НКА
