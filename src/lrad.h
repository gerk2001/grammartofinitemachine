﻿#ifndef LRAD_H
#define LRAD_H

#include <fmt/color.h>
#include <fmt/core.h>
#include <grammar.h>
#include <list>
#include <lran.h>
#include <set>
#include <sstream>
#include <stack>
#include <unordered_set>

struct Rule {
    char nonterm;
    size_t body_length;
};

//структура, реализующая содержимое одной строки ДКА
struct DRow {
    std::unordered_map<char, size_t> jumpby;
    std::list<Rule> bundles;
};

//класс, реализующий преобразование НКА в ДКА?
class LRAD {
public:
    using derivation = std::vector<std::pair<std::string, std::string>>;
    LRAD(const Grammar& grammar, const LRAN& lran);
    //интерфейс для ввода слов на проверку автомату
    void analyzeStrings();
    bool string_match(derivation& d_tree, std::string line, bool normal_mode = true, std::vector<size_t> extra_states = {}, std::vector<char> extra_symbols = {});

private:
    std::vector<std::pair<char, std::string>> _grammar_rules;
    char _begin_ch, _end_ch;
    std::vector<NRow> _lran_table;
    std::unordered_set<char> _lran_symbols;
    std::vector<DRow> _table;

    //операции над состояниями НКА:
    //1. ε-замыкание для состояния s
    std::set<size_t> epsilonClosure(size_t s);
    //2. ε-замыкание для множества состояний T
    std::set<size_t> epsilonClosure(std::set<size_t> T);
    //3. множество состояний, в которые возможен переход по символу a из множества состояний T (a = DSymbols[k])
    std::set<size_t> move(std::set<size_t> T, char a);
    void makeDerivationTree(derivation& tree, const std::vector<char>& symbols, const std::string& line, char nonterm = ' ', std::string body = "", size_t pos = -1);
    void printDerivationTree(derivation& tree);
    //возвращает кортеж <флаг допуска, количество символов в продукции, нетерминал> для свертки
    //если квазисостояние допускает только одну свертку, то возвращаются данные для неё
    //если квазисостояние допускает n сверток, то последовательно проверяется n-1 сверток
    //если найдена свертка, с которой принимается исходная строка, то возвращаем данные для неё
    //если просмотрено n-1 свёрток и ни одна из них не подошла, то возвращаем данные для последней
    //флаг допуска true, если вариантов свертки было > 1 и при их анализе входная цепочка была принята
    const std::tuple<bool, std::string, char> getConvolution(derivation& d_tree, std::string line, size_t state_now, const std::vector<size_t>& states, const std::vector<char>& symbols);
    //алгоритм проверки слова заданным автоматом
    //если среди номеров состояний НКА, составляющих состояние ДКА, есть
    //номер, соответствующий квазисостоянию, то присваиваем его полю qstate_id
    //если состояние ДКА может интерпретироваться не как квазисостояние, то can_be_state = true
    void printTable(const std::vector<std::set<size_t>>& assist_table);
    //генерация таблицы детерминированного автомата по таблице недетерминированного
    void generateTable();
};

#endif //LRAD_H
