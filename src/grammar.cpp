﻿#include <grammar.h>

void Grammar::deleteNontermFromAll(char epsilon_nonterm)
{
    _rules.erase(epsilon_nonterm);

    _epsilon_nonterms.erase(epsilon_nonterm);

    for (size_t i = 0; i < _nonterms.size(); i++)
        if (_nonterms[i] == epsilon_nonterm) {
            _nonterms.erase(_nonterms.begin() + i);
            break;
        }

    for (auto& each_row : _rules) {
        std::unordered_set<std::string> mod_bodies;
        auto it = each_row.second.begin();
        while (it != each_row.second.end()) {
            std::string each_body = *it;
            size_t i = 0;
            bool contains_ε = false;
            while (i < each_body.length())
                if (each_body[i] == epsilon_nonterm) {
                    each_body.erase(i, 1);
                    contains_ε = true;
                } else
                    ++i;

            if (contains_ε) {
                if (each_body.length() == 0)
                    each_body = "э";
                mod_bodies.insert(each_body);
                it = each_row.second.erase(it);
            } else
                ++it;
        }
        each_row.second.insert(mod_bodies.begin(), mod_bodies.end());
    }
}

void Grammar::deleteEpsilonProductions()
{
    for (auto& each : _rules)
        each.second.erase("э");
}

void Grammar::createSetOfEpsilonNonterms()
{
    for (auto it = _rules.begin(); it != _rules.end();) {
        auto& [nonterm, bodies] = *it;
        if (bodies.contains("э")) {
            //вырожденный случай, когда нетерминал содержит только ε-переход
            if (bodies.size() == 1) {
                deleteNontermFromAll(nonterm);
                it = _rules.begin();
            } else {
                _epsilon_nonterms.insert(nonterm);
                it++;
            }
        } else
            it++;
    }
}

void Grammar::makeNewCombinations(std::string new_body, size_t i, const std::string& body, std::unordered_set<std::string>& new_bodies)
{
    if (i < body.length())
        if (_epsilon_nonterms.contains(body[i])) {
            makeNewCombinations(new_body + body[i], i + 1, body, new_bodies);
            makeNewCombinations(new_body, i + 1, body, new_bodies);
        } else
            makeNewCombinations(new_body + body[i], i + 1, body, new_bodies);
    else if (new_body.length() != 0)
        new_bodies.insert(new_body);
}

void Grammar::replacementForProduction()
{
    for (auto& each_row : _rules) {
        std::unordered_set<std::string> new_bodies;
        for (auto& each_body : each_row.second)
            makeNewCombinations("", 0, each_body, new_bodies);
        each_row.second = new_bodies;
    }
}

void Grammar::extraChecking()
{
    auto it = _rules.begin();
    while (it != _rules.end()) {
        auto& [nonterm, bodies] = *it;
        if (!_epsilon_nonterms.contains(nonterm)) {
            bool is_epsilon_nonterm = false;
            for (auto each = bodies.begin(); each != bodies.end() && !is_epsilon_nonterm; each++) {
                std::string body = *each;
                auto n = body.length();
                size_t epsilon_ch_count = 0;
                for (size_t k = 0; k < n; k++)
                    if (_epsilon_nonterms.contains(body[k]))
                        epsilon_ch_count++;
                    else
                        k = n;
                if (epsilon_ch_count == n)
                    is_epsilon_nonterm = true;
            }

            if (is_epsilon_nonterm) {
                _epsilon_nonterms.insert(nonterm);
                it = _rules.begin();
            } else
                it++;
        } else
            it++;
    }
}

void Grammar::moveStartNontermToFirstPlace()
{
    for (size_t i = 0; i < _nonterms.size(); i++)
        if (_nonterms[i] == _begin_ch) {
            while (i > 0) {
                std::swap(_nonterms[i - 1], _nonterms[i]);
                i--;
            }
            break;
        }
}

void Grammar::validateStartNonterm()
{
    if (_rules.contains(_begin_ch)) {
        std::string new_body;
        if (_rules[_begin_ch].size() == 1) {
            new_body = *_rules[_begin_ch].begin() /* + _end_ch*/;
            moveStartNontermToFirstPlace();
        } else {
            new_body = std::string(1, _begin_ch) /* + _end_ch*/;
            _begin_ch = 'F'; //TODO: функция, генерирующая нетерминал-замену
            _nonterms.insert(_nonterms.begin(), _begin_ch);
        }
        _rules[_begin_ch] = { new_body };
    } else {
        _rules = { { _begin_ch, { std::string(1, _end_ch) } } };

        _nonterms = { _begin_ch };
        _nonterms.shrink_to_fit();

        _epsilon_nonterms.clear();
    }
}

void Grammar::addRule(std::string line)
{
    char nonterm = line[0];
    if (!_rules.contains(nonterm))
        _nonterms.push_back(nonterm);
    line.erase(0, 3);
    line += "|";
    while (line.find("|") != std::string::npos) {
        auto i = line.find("|");
        _rules[nonterm].insert(line.substr(0, i));
        line.erase(0, i + 1);
    }
}

void Grammar::input()
{
    std::string line;
    fmt::print("\n\tВведите стартовый нетерминал(\"|\" и \"•\" использовать запрещено): ");
    while (true) {
        getline(std::cin, line);
        if (line.length() == 1 && line[0] != '|' && line[0] != '•') {
            _begin_ch = line[0];
            break;
        } else
            fmt::print(fmt::emphasis::bold | fg(fmt::color::red),
                    "\tЗапрещенный нетерминал!Повторите ввод : ");
    }
    std::cout << "\tВведите список продукций через Enter. \n\tДля объединения в альтернативы используйте |, для завершения ввода \"end\"\n\t";
    std::regex mask("[^\\|\\•]->[^\\|]+(\\|[^\\|]+)*");
    while (true) {
        getline(std::cin, line);
        if (line == "end")
            break;
        else if (std::regex_match(line, mask))
            addRule(line);
        else
            fmt::print(fmt::emphasis::bold | fg(fmt::color::red),
                    "\tНеверные данные! Повторите ввод\n");
        std::cout << "\t";
    }
}

void Grammar::process()
{
    //преобразование к виду, не содержащему ε-правил
    createSetOfEpsilonNonterms();
        extraChecking();
    deleteEpsilonProductions();
    replacementForProduction();

    validateStartNonterm();
}

Grammar::Grammar(char s0, const std::vector<std::string>& rules)
{
    _begin_ch = s0;
    for (const auto& each_of : rules)
        addRule(each_of);
    process();
}

Grammar::Grammar() {
    input();
    process();
}

Grammar::~Grammar() {

}

std::vector<std::pair<char, std::string>> Grammar::getListOfRules() const
{
    std::vector<std::pair<char, std::string>> expanded_list_of_rules;
    for (auto& each : _nonterms)
        for (auto& body : _rules.at(each))
            expanded_list_of_rules.push_back({ each, body });
    return expanded_list_of_rules;
}

std::unordered_set<char> Grammar::getListOfNonterms() const
{
    std::unordered_set<char> Nonterms(_nonterms.begin(), _nonterms.end());
    return Nonterms;
}
