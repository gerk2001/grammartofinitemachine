﻿#ifndef GRAMMAR_H
#define GRAMMAR_H

#include <fmt/core.h>
#include <fmt/color.h>
#include <iostream>
#include <regex>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

//TODO: поработать над классом продукций. э-правила и политика их обработки
class Grammar {
public:
    Grammar(char s0, const std::vector<std::string>& rules);
    Grammar();
    ~Grammar();
    //получение таблицы продукций в развернутом виде (альтернативы раскрываются как отдельные продукции)
    //NOTE: если в векторе нетерминалов есть ключи, по которым
    //нет записей в таблице, вызывается исключение std::out_of_range
    std::vector<std::pair<char, std::string>> getListOfRules() const;
    //получение списка нетерминалов
    //на первом месте гарантируется нахождение стартового нетерминала
    std::unordered_set<char> getListOfNonterms() const;
    auto getBeginCh() const { return _begin_ch; }
    auto getEndCh() const { return _end_ch; }

private:
    std::unordered_map<char, std::unordered_set<std::string>> _rules;
    std::vector<char> _nonterms;
    std::unordered_set<char> _epsilon_nonterms;
    char _begin_ch = 'S';
    char _end_ch = '•';

    //удаление нетерминала из множества нетерминалов и из всех содержащих этот нетерминал правых частей грамматики
    void deleteNontermFromAll(char epsilon_nonterm);
    //удаление ε-правил из исходной грамматики
    void deleteEpsilonProductions();
    //построение множества ε-порождающих нетерминалов вида A->ε
    void createSetOfEpsilonNonterms();
    //создание множества сочетаний для одной продукции
    void makeNewCombinations(std::string new_body, size_t i, const std::string& body, std::unordered_set<std::string>& new_bodies);
    //создание множества новых продукций из существующих для всей грамматики
    void replacementForProduction();
    //поиск ε-порождающих нетерминалов вида B->A1An, Ai->ε. TODО: добавить структурную привязку для удобочитаемости
    void extraChecking();
    void moveStartNontermToFirstPlace();
    //если начальных вершин больше одной, то вводится новый стартовый нетерминал
    //предыдущий стартовый нетерминал становится единственным его правилом
    //если в процессе преобразований не осталось правил со стартовым нетерминалом
    //то все правила удаляются, добавляется единственное правило _begin_ch->[ε]+_end_ch
    //список нетерминалов так же очищается, в нём остается только стартовый нетерминал
    void validateStartNonterm();
    //добавление пары нетерминал - множество его правых частей
    void addRule(std::string line);
    //ввод и его предварительная проверка на соответствие шаблону требуемых данных
    void input();
    //предварительная обработка грамматики
    void process();
};

#endif //GRAMMAR_H
