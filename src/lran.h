﻿#ifndef LRAN_H
#define LRAN_H

#include <grammar.h>
#include <set>
#include <fmt/color.h>
#include <fmt/core.h>

//структура, содержащая поля одной строки НКА
struct NRow {
    std::pair<size_t, size_t> NState;
    std::unordered_map<char, std::set<size_t>> NTrans;
    bool is_quasistential;
};

//класс, реализующий преобразование списка продукций в НКА?
class LRAN {
public:
    LRAN(const Grammar& grammar);
    ~LRAN();
    //вывод таблицы НКА построчно
    void printTable();
    //возврат таблицы НКА
    auto getLranTable() const { return _table; }
    //возврат множества символов переходов в таблице НКА (ε-символ в него не включен)
    std::unordered_set<char> getLranSymbols() const;

private:
    std::vector<NRow> _table;
    std::vector<std::pair<char, std::string>> _grammar_rules;
    std::unordered_set<char> _grammar_nonterms;
    std::unordered_set<char> _symbols;

    //получение множества номеров строк, соответствующих состояниям {0,Y}  в таблице, в левой части продукций которых стоит нетерминал nonterm
    std::set<size_t> getSetOfEpsilonTransitionsFor(char nonterm, size_t self);
    //получение номера строки, соответствующей паре состоянию{X,Y}, в таблице НКА. =n - строки нет
    size_t getRowIndex(std::pair<size_t, size_t> state);
    //расширение таблицы переходов(при добавлении колонки для нового символа)
    void enlargeTransTable(char symbol);
    //добавление множества переходов по символу symbol для состояния {X,Y}
    void adjumpbyitionsFor(std::pair<size_t, size_t> state, char _Symbol, char _Nonterm = '\0');
    //обработка продукций и занесение результата в таблицу НКА
    void generateTable();
};

#endif //LRAN_H
