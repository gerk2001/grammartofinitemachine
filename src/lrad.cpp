﻿#include <lrad.h>

LRAD::LRAD(const Grammar& grammar, const LRAN& lran)
    : _grammar_rules(grammar.getListOfRules())
    , _begin_ch(grammar.getBeginCh())
    , _end_ch(grammar.getEndCh())
    , _lran_table(lran.getLranTable())
    , _lran_symbols(lran.getLranSymbols())
{
    generateTable();
}

std::set<size_t> LRAD::epsilonClosure(size_t s)
{
    std::set<size_t> epsilon_closure = _lran_table[s].NTrans['э'];

    std::stack<size_t> epsilon_trans;
    for (auto& each : epsilon_closure)
        epsilon_trans.push(each);

    epsilon_closure.insert(s);

    while (!epsilon_trans.empty()) {
        auto t = epsilon_trans.top();
        epsilon_trans.pop();
        for (auto& each_from : _lran_table[t].NTrans['э'])
            if (!epsilon_closure.contains(each_from)) {
                epsilon_closure.insert(each_from);
                epsilon_trans.push(each_from);
            }
    }
    return epsilon_closure;
}

std::set<size_t> LRAD::epsilonClosure(std::set<size_t> T)
{
    std::set<size_t> epsilon_closure;
    for (auto& s : T) {
        std::set<size_t> epsilon_cl_part = epsilonClosure(s);
        epsilon_closure.insert(epsilon_cl_part.begin(), epsilon_cl_part.end());
    }
    return epsilon_closure;
}

std::set<size_t> LRAD::move(std::set<size_t> T, char a)
{
    std::set<size_t> a_closure;
    for (auto& each_from : T) {
        std::set<size_t> a_cl_part = _lran_table[each_from].NTrans[a];
        a_closure.insert(a_cl_part.begin(), a_cl_part.end());
    }
    return a_closure;
}

void LRAD::makeDerivationTree(derivation& tree, const std::vector<char>& symbols, const std::string& line, char nonterm, std::string body, size_t pos)
{
    std::string result = "";
    for (auto& each : symbols)
        result += each;
    result += line;
    std::string rule = std::string(1, nonterm) + "->" + body;
    if (pos == -1)
        tree.push_back({ rule, result });
    else
        tree.insert(tree.begin() + pos, { rule, result });
}

void LRAD::printDerivationTree(derivation& tree)
{
    auto it = tree.crbegin();
    auto end = tree.crend();
    while (it != end) {
        auto& [rule, result] = *it;
        fmt::print("\t{} | {}\n", rule, result);
        it++;
    }
}

const std::tuple<bool, std::string, char> LRAD::getConvolution(derivation& d_tree, std::string line, size_t state_now, const std::vector<size_t>& states, const std::vector<char>& symbols)
{
    auto possible_bundles = _table[state_now].bundles;
    auto extractBody = [&symbols](size_t k) {
        return std::string(symbols.end() - k, symbols.end());
    };

    auto bundles_iterator = possible_bundles.begin();
    if (auto n = possible_bundles.size(); n == 1) {
        auto& [top_x, body_length] = possible_bundles.front();
        return { false, extractBody(body_length), top_x };
    } else {
        for (size_t i = 0; i < n - 1; i++) {
            auto& [top_x, k] = *bundles_iterator;
            std::vector<char> extra_symbols(symbols.begin(), symbols.end() - k);
            extra_symbols.push_back(top_x);
            std::vector<size_t> extra_states(states.begin(), states.end() - (k - 1));
            auto pos = d_tree.size();
            if (string_match(d_tree, line, false, extra_states, extra_symbols)) {
                auto body = extractBody(k);
                makeDerivationTree(d_tree, symbols, line, top_x, body, pos); //вывод
                return { true, body, top_x };
            }
            bundles_iterator++;
        }

        auto& [top_x, k] = *bundles_iterator;
        return { false, extractBody(k), top_x };
    }
}

bool LRAD::string_match(derivation& d_tree, std::string line, bool normal_mode, std::vector<size_t> extra_states, std::vector<char> extra_symbols)
{
    derivation tree;
    std::vector<size_t> states;
    std::vector<char> symbols;
    if (normal_mode) {
        states.push_back(1);
        symbols.push_back(line[0]);
        line.erase(0, 1);
    } else {
        states = extra_states;
        symbols = extra_symbols;
    }

    while (true) {
        auto s = states.back();
        char x = symbols.back();
        if (symbols.size() == 1 && line.size() == 0 && x == _begin_ch) {
            d_tree.insert(d_tree.end(), tree.begin(), tree.end()); //вывод
            d_tree.push_back({ "", std::string(1, x) }); //вывод
            return true;
        } else if (size_t state_now = _table[s].jumpby[x]; state_now == 0)
            return false;
        else if (!_table[state_now].bundles.empty() && (line.empty() || (!line.empty() && _table[state_now].jumpby[line[0]] == 0))) {
            auto& [already_accepted, body, nonterm] = getConvolution(tree, line, state_now, states, symbols);
            if (already_accepted) {
                d_tree.insert(d_tree.end(), tree.begin(), tree.end()); //вывод
                return true;
            } else {
                auto k = body.size();
                makeDerivationTree(tree, symbols, line, nonterm, body); //вывод
                symbols.erase(symbols.end() - k, symbols.end());
                symbols.push_back(nonterm);
                states.erase(states.end() - (k - 1), states.end());
            }
        } else {
            states.push_back(state_now);
            symbols.push_back(line[0]);
            line.erase(0, 1);
        }
    }
}

void LRAD::generateTable()
{
    std::vector<std::set<size_t>> assist_table;

    auto getRowIndex = [&assist_table](auto state) {
        auto n = assist_table.size();
        for (size_t i = 0; i < n; i++)
            if (assist_table[i] == state)
                return i;

        assist_table.push_back(state);
        return n;
    };
    auto getAllBundles = [this](auto state) {
        std::list<Rule> bundles;
        for (auto& each : state)
            if (_lran_table[each].is_quasistential) {
                auto prod_ix = _lran_table[each].NState.second;
                auto& [nonterm, body] = _grammar_rules[prod_ix];
                bundles.push_back({ nonterm, body.size() });
            }
        return bundles;
    };

    _table.push_back({});
    assist_table.push_back({});
    assist_table.push_back(epsilonClosure(0));

    _table.push_back({ {}, {} });

    for (size_t i = 0; i < _table.size(); i++) {
        for (const auto& each_from : _lran_symbols) {
            std::set<size_t> set_of_trans = epsilonClosure(move(assist_table[i], each_from));
            _table[i].jumpby[each_from] = getRowIndex(set_of_trans);
            if (_table[i].jumpby[each_from] == _table.size())
                _table.push_back({ {}, { getAllBundles(set_of_trans) } });
        }
    }

    printTable(assist_table);
}

void LRAD::printTable(const std::vector<std::set<size_t>>& assist_table)
{
    fmt::print("\n\tАвтоматная таблица ДКА\n"
               "\t0 <error>\n");

    for (size_t i = 1; i < _table.size(); i++) {
        fmt::print("\t{} ", i);

        if (assist_table[i].size() > _table[i].bundles.size())
            for (auto& each : _lran_symbols)
                fmt::print("'{}' -> {} ", each, _table[i].jumpby[each]);

        if (!_table[i].bundles.empty())
            for (auto& each : assist_table[i])
                if (_lran_table[each].is_quasistential) {
                    auto prod_ix = _lran_table[each].NState.second;
                    auto [nonterm, body] = _grammar_rules[prod_ix];
                    fmt::print(fmt::emphasis::bold | fg(fmt::color::gray),
                        "<{}->{}>", nonterm, body);
                }
        std::cout << "\n";
    }
}

void LRAD::analyzeStrings()
{
    fmt::print("\n\tВведите слова для анализа через Enter."
               "\n\tДля завершения ввода используйте \"end\"\n\t");
    std::string line;

    while (true) {
        getline(std::cin, line);
        if (line == "end")
            break;
        else if (derivation tree; string_match(tree, line /*+ _end_ch*/)) {
            printDerivationTree(tree);
            fmt::print(fmt::emphasis::bold | fg(fmt::color::lime_green),
                "\tСлово \"{}\" принимается автоматом\n\n\t", line);
        } else
            fmt::print(fmt::emphasis::bold | fg(fmt::color::red),
                "\tСлово \"{}\" не принимается автоматом\n\n\t", line);
    }
}
