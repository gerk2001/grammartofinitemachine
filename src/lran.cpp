﻿#include <lran.h>

std::set<size_t> LRAN::getSetOfEpsilonTransitionsFor(char nonterm, size_t self)
{
    std::set<size_t> epsilon_trans;
    auto n = _grammar_rules.size();
    for (size_t i = 0; i < n; i++)
        if (_grammar_rules[i].first == nonterm && i != self) {
            epsilon_trans.insert(i);
        }

    return epsilon_trans;
}

size_t LRAN::getRowIndex(std::pair<size_t, size_t> state)
{
    auto n = _table.size();
    for (size_t i = 0; i < n; i++)
        if (_table[i].NState.first == state.first && _table[i].NState.second == state.second)
            return i;
    return n;
}

void LRAN::enlargeTransTable(char symbol)
{
    auto n = _table.size();
    for (size_t i = 0; i < n; i++)
        //if (!_table[i].NTrans.contains(symbol)) вроде как гарантируется, что при запуске этой функции символа в таблице не было
        _table[i].NTrans[symbol] = {};
}

void LRAN::adjumpbyitionsFor(std::pair<size_t, size_t> state, char _Symbol, char _Nonterm)
{

    //если символа нет в множестве символов
    if (!_symbols.contains(_Symbol)) {
        enlargeTransTable(_Symbol);
        _symbols.insert(_Symbol);
    }

    auto iR = getRowIndex(state);

    if (_Symbol == 'э')
        _table[iR].NTrans['э'] = getSetOfEpsilonTransitionsFor(_Nonterm, iR);
    else {
        auto [point, prod_ix] = state;
        auto iNR = getRowIndex({ point + 1, prod_ix });
        _table[iR].NTrans[_Symbol].insert(iNR);

        //если строки, соответствующей состоянию {X+1,Y}, нет в таблице НКА, то добавляем её
        if (iNR == _table.size()) {
            _table.push_back({ { point + 1, prod_ix }, {}, false });
            for (auto& each : _symbols)
                _table[iNR].NTrans[each] = {};
        }
    }
}

void LRAN::generateTable()
{
    for (size_t i = 0; i < _table.size(); i++) {
        auto [point, prod_ix] = _table[i].NState;
        std::string body = _grammar_rules[prod_ix].second;
        if (point < body.length()) {
            char symbol = body[point];
            if (_grammar_nonterms.contains(symbol)) {
                adjumpbyitionsFor(_table[i].NState, symbol);
                adjumpbyitionsFor(_table[i].NState, 'э', symbol);
            } else
                adjumpbyitionsFor(_table[i].NState, symbol);
        } else
            _table[i].is_quasistential = true;
    }
}

LRAN::LRAN(const Grammar& grammar)
    : _grammar_rules(grammar.getListOfRules())
    , _grammar_nonterms(grammar.getListOfNonterms())
{
    auto n = _grammar_rules.size();
    for (size_t i = 0; i < n; i++)
        _table.push_back({ { 0, i }, {}, false });

    generateTable();
}

LRAN::~LRAN()
{
}

void LRAN::printTable()
{
    fmt::print("\n\tАвтоматная таблица НКА\n");
    for (auto& each_row : _table) {
        auto [point, prod_ix] = each_row.NState;
        fmt::print("\t({}:{}) ", each_row.NState.first, each_row.NState.second);
        if (!each_row.is_quasistential) {
            for (auto& each_symbol : _symbols) {
                if (!each_row.NTrans[each_symbol].empty())
                    fmt::print(" '{}' -> ", each_symbol);
                for (auto& trans : each_row.NTrans[each_symbol])
                    fmt::print("({}:{}) ", _table[trans].NState.first, _table[trans].NState.second);
            }
        } else
            fmt::print(fmt::emphasis::bold | fg(fmt::color::gray),
                    "<{}->{}>", _grammar_rules[prod_ix].first, _grammar_rules[prod_ix].second);
        std::cout << "\n";
    }
}

std::unordered_set<char> LRAN::getLranSymbols() const
{
    std::unordered_set<char> lran_symbols = _symbols;
    lran_symbols.erase('э');
        return lran_symbols;
}
